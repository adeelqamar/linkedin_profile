<?php

$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('customer');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute('customer', 'linkedin_profile', array(
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Linkedin profile url',
    'user_defined'  => 1,
    'required'   => 0,
    'visible'    => 0
));

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'linkedin_profile',
    '100'
);

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
$attribute->setData('used_in_forms', array(
    'customer_account_create',
    'customer_account_edit',
    'adminhtml_customer'
));
$attribute->save();

$installer->endSetup();