<?php

class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Abstract
{    
    public function getConfig()
    {
        return Mage::getStoreConfig('customer/address/linkedin_profile_show');
    }
}